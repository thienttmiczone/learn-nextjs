export interface NotFoundPageProps {}

export default function NotFoundPage(props: NotFoundPageProps) {
  return <h1>404 Page Not Found</h1>;
}
