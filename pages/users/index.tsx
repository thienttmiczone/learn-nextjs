import React from 'react';

interface UserInterface {
  id: number;
  name: string;
  email: string;
}

interface UsersInterface {
  userList: UserInterface[];
}

export default function Users(props: UsersInterface) {
  const { userList } = props;
  return (
    <>
      <h1>List of user</h1>
      {userList?.map((user) => {
        return (
          <div key={user.id}>
            <p>{user.name}</p>
            <p>{user.email}</p>
          </div>
        );
      })}
    </>
  );
}

export async function getStaticProps() {
  const response = await fetch('https://jsonplaceholder.typicode.com/users');
  const data = await response.json();

  return {
    props: {
      userList: data,
    },
  };
}
