module.exports = {
  extends: 'next/core-web-vitals',
  rules: {
    'react-hooks/exhaustive-deps': 0, // issue: https://github.com/facebook/react/issues/14920
  },
};
